package com.mycompany.l01;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class AdminTest extends FirstTest {

    SoftAssert softAssert = new SoftAssert();

    @Test(description = "Admin: Create a user")
    public void adminCreateUser() {

        //Login as admin

        //user
        WebElement userNameInput = driver.findElement(By.id("user_login"));
        userNameInput.sendKeys("admin");

        //password
        WebElement passwordInput = driver.findElement(By.id("user_pass"));
        passwordInput.sendKeys("kCb1hRpr");

        //click login
        WebElement btnLogin = driver.findElement(By.xpath("//*[@id=\"wp-submit\"]"));
        btnLogin.click();

        softAssert.assertNotNull(driver.findElement(By.id("adminmenu")));

        //Create random user
        RandomUser randomUser = new RandomUser();
        Actions actions = new Actions(driver);
        WebDriverWait wait = new WebDriverWait(driver, 15);

        //create user

        WebElement adminMenuCreateUser = driver.findElement(By.xpath("//*[@id=\"menu-users\"]/a"));
        adminMenuCreateUser.click();

        WebElement addNewUserLink = driver.findElement(By.xpath("//*[@id=\"wpbody-content\"]/div[3]/a"));
        addNewUserLink.click();


        String userName = randomUser.name;

        userNameInput = driver.findElement(By.xpath("//*[@id=\"user_login\"]"));
        userNameInput.sendKeys(userName);

        WebElement userEmailInput = driver.findElement(By.xpath("//*[@id=\"email\"]"));
        userEmailInput.sendKeys(randomUser.email);

        WebElement userFirstNameInput = driver.findElement(By.xpath("//*[@id=\"first_name\"]"));
        userFirstNameInput.sendKeys(randomUser.name);

        WebElement userLastNameInput = driver.findElement(By.xpath("//*[@id=\"last_name\"]"));
        userLastNameInput.sendKeys(randomUser.lastname);

        WebElement userWebsite = driver.findElement(By.xpath("//*[@id=\"url\"]"));
        userWebsite.sendKeys(randomUser.website);

        WebElement btnShowPass = driver.findElement(By.xpath("//*[@id=\"createuser\"]/table/tbody/tr[6]/td/button"));
        btnShowPass.click();

        //password
        wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//*[@id=\"pass1\"]"))));
        String userPassword = driver.findElement(By.xpath("//*[@id=\"pass1\"]")).getAttribute("value");
        softAssert.assertNotNull(userPassword, "New user password - null");

        WebElement addNewUser = driver.findElement(By.xpath("//input[@type = \"submit\"]"));
        addNewUser.click();


        WebElement adminMenuPopUp = driver.findElement(By.xpath("//li[@id=\"wp-admin-bar-my-account\"]"));
        WebElement logout = driver.findElement(By.xpath("//*[@id=\"wp-admin-bar-logout\"]/a"));

        actions.moveToElement(adminMenuPopUp).perform();

        wait.until(ExpectedConditions.elementToBeClickable(logout));


        if(logout.isDisplayed()) {
            logout.click();
        }else
            softAssert.assertTrue(logout.isDisplayed());


        //login as created user
        user = "user";
        getBaseUrl();

        userNameInput = driver.findElement(By.cssSelector("input#username"));
        userNameInput.sendKeys(userName);

        //password
        passwordInput = driver.findElement(By.cssSelector("input#password"));
        passwordInput.sendKeys(userPassword);

        //click login
        btnLogin = driver.findElement(By.xpath("//button[@name='login']"));
        btnLogin.click();

        softAssert.assertTrue(driver.findElement(By.xpath("//strong[1]")).getText().contains(userName));

        //logout
        logout = driver.findElement(By.linkText("Выйти"));
        logout.click();

        softAssert.assertAll();

    }

}

