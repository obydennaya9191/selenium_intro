package com.mycompany.l01;

import java.util.Random;

public class RandomUser extends FirstTest {

    String chars = "abcdefghijklmnopqrstuvwxyz0123456789";

    String email = randomMail();
    String name = randomName();
    String lastname = randomLastname();
    String website = randomWebsite();

    public static String randomString(String chars, int length) {
        Random rand = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<length; i++) {
            sb.append(chars.charAt(rand.nextInt(chars.length())));
        }
        return sb.toString();
    }

    public String randomMail() {
        return randomString(chars , 7) + "@mail.mail";

    }

    public String randomName() {
        return randomString(chars,7);

    }

    public String randomLastname() {
        return randomString(chars,7);
    }

    public String randomWebsite() {
        return randomString(chars,5) + ".domain";


    }


}
