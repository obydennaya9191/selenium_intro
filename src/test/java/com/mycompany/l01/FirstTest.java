package com.mycompany.l01;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.testng.annotations.*;

import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

public class FirstTest {

    WebDriver driver = null;
    String testAppUrl = "http://cw07529-wordpress.tw1.ru";
    String current_env = "chrome";
    String baseUrl = testAppUrl;
    String user = "admin";

    @BeforeTest
    public void initBrowser() {

        if (current_env.equals("firefox")) {
            String pathToGeckoDriver = Paths.get("C:\\webdrivers\\geckodriver.exe").toAbsolutePath().toString();
            System.setProperty("webdriver.gecko.driver", pathToGeckoDriver);
            driver = new FirefoxDriver();
            driver.manage().window().maximize();
        }
        else if (current_env.equals("chrome")){
            System.setProperty("webdriver.chrome.driver","C:\\webdrivers\\chromedriver.exe");
            driver = new ChromeDriver();
            driver.manage().window().maximize();
        }
        else if (current_env.equals("opera")){
            System.setProperty("webdriver.opera.driver","C:\\webdrivers\\operadriver.exe");
            driver = new OperaDriver();
            driver.manage().window().maximize();
        }
        else
        {
            System.exit(0);
        }
        driver.get(getBaseUrl());
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);

    }

    public String getBaseUrl() {

        if(user.equals("user")) {
            baseUrl = testAppUrl + "/my-account/";

        }else if(user.equals("admin")) {
            baseUrl = testAppUrl + "/wp-login.php";

        }
        driver.get(baseUrl);
        return baseUrl;
    }

    @AfterTest()
    public void closeBrowser(){
        driver.quit();
    }
}
